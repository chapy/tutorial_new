-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Apr 17, 2017 at 06:02 AM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+02:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lventonl_hebrew`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE IF NOT EXISTS `admin_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `birthday` varchar(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`id`, `admin_id`, `birthday`) VALUES
(1, 4545, '1989');

-- --------------------------------------------------------

--
-- Table structure for table `create_account`
--

CREATE TABLE IF NOT EXISTS `create_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `role` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_birth` varchar(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `create_account`
--

INSERT INTO `create_account` (`id`, `name`, `role`, `user_id`, `date_birth`) VALUES
(6, 'sehar', 'CEO', 65456, '2535'),
(7, 'jdsjhb', 'basic', 545120, '1545'),
(9, 'hammad', 'head ofpackaging', 11, '12345'),
(10, 'aj', 'hrad of HR', 45, '789'),
(11, 'hbk', 'security manager', 456, '12313'),
(12, 'rko', 'it manager', 33, '354'),
(13, 'waqar', 'packaging worker', 66, '666'),
(14, 'hani', 'packaging 2', 777, '8888');

-- --------------------------------------------------------

--
-- Table structure for table `final-result`
--

CREATE TABLE IF NOT EXISTS `final-result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(11) NOT NULL,
  `packageid` varchar(30) NOT NULL,
  `result_percent` varchar(150) NOT NULL,
  `time_period` varchar(250) NOT NULL,
  `update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `final-result`
--

INSERT INTO `final-result` (`id`, `userid`, `packageid`, `result_percent`, `time_period`, `update`) VALUES
(1, '11', '75', '50%', '3', '2017-03-27 14:51:59'),
(2, '11', '74', '0%', '3', '2017-03-27 16:41:27'),
(4, '11', '84', '0%', '3', '2017-03-30 17:05:57'),
(5, '11', '83', '0%', '3', '2017-03-30 17:06:08'),
(6, '11', '82', '100%', '12', '2017-03-30 17:06:19'),
(9, '11', '85', '100%', '12', '2017-03-30 19:13:59'),
(10, '11', '87', '100%', '12', '2017-04-02 16:46:52');

-- --------------------------------------------------------

--
-- Table structure for table `heb`
--

CREATE TABLE IF NOT EXISTS `heb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ced` varchar(200) NOT NULL,
  `erg` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE IF NOT EXISTS `result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `result` varchar(30) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`id`, `user_id`, `question_id`, `result`, `time`) VALUES
(1, 11, 115, 'False', '2017-03-27 14:51:59'),
(2, 11, 121, 'True', '2017-03-27 14:51:59'),
(3, 11, 116, 'False', '2017-03-27 16:41:27'),
(4, 11, 122, 'False', '2017-03-27 16:41:27'),
(7, 11, 126, 'False', '2017-03-30 17:05:57'),
(8, 11, 128, 'False', '2017-03-30 17:05:57'),
(9, 11, 127, 'False', '2017-03-30 17:06:08'),
(10, 11, 125, 'True', '2017-03-30 17:06:19'),
(13, 11, 129, 'True', '2017-03-30 19:13:59'),
(14, 11, 130, 'True', '2017-03-30 19:13:59'),
(15, 11, 132, 'True', '2017-04-02 16:46:52');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pkg`
--

CREATE TABLE IF NOT EXISTS `tbl_pkg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_work` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `b_basic` varchar(200) NOT NULL,
  `b_CEO` varchar(200) NOT NULL,
  `b_hop` varchar(200) NOT NULL,
  `b_hrad_HR` varchar(200) NOT NULL,
  `b_sm` varchar(200) NOT NULL,
  `b_itm` varchar(200) NOT NULL,
  `b_pw` varchar(200) NOT NULL,
  `b_p2` varchar(200) NOT NULL,
  `t_basic` varchar(200) NOT NULL,
  `t_CEO` varchar(200) NOT NULL,
  `t_hop` varchar(200) NOT NULL,
  `t_hrad` varchar(200) NOT NULL,
  `t_sm` varchar(200) NOT NULL,
  `t_itm` varchar(200) NOT NULL,
  `t_pw` varchar(200) NOT NULL,
  `t_p2` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=88 ;

--
-- Dumping data for table `tbl_pkg`
--

INSERT INTO `tbl_pkg` (`id`, `name_work`, `image`, `b_basic`, `b_CEO`, `b_hop`, `b_hrad_HR`, `b_sm`, `b_itm`, `b_pw`, `b_p2`, `t_basic`, `t_CEO`, `t_hop`, `t_hrad`, `t_sm`, `t_itm`, `t_pw`, `t_p2`) VALUES
(72, 'File Treatment', '1489565462_1489484845_oop_in_php_tutorial.pdf', '', 'blu_CEO', 'blu_head ofpackaging', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(73, 'File Treatment', '1489565589_1489485867_0672325616.pdf', '', '', '', 'blu_hrad of HR', 'blu_security manager', 'blu_it manager', '', '', '', '', '', '', '', '', '', ''),
(74, 'Chemical Process', '1489565781_1489485867_0672325616.pdf', '', 'blu_CEO', 'blu_head ofpackaging', 'blu_hrad of HR', '', '', 'blu_packaging worker', '', 'tree_basic', '', '', '', '', '', '', ''),
(75, 'Security', '1489566201_1489482299_oop_in_php_tutorial.pdf', 'blu_basic', '', 'blu_head ofpackaging', '', '', '', '', '', '', '', 'tree_head ofpackaging', '', '', '', '', ''),
(76, 'Chemical Process', '1489820820_1489482299_oop_in_php_tutorial.pdf', '', '', '', '', 'blu_security manager', '', '', '', 'tree_basic', 'tree_CEO', 'tree_head ofpackaging', '', '', '', '', ''),
(80, 'this is a test', '1490426888_29073280966-604376461-ticket.pdf', '', '', '', '', '', '', '', '', 'tree_basic', 'tree_CEO', 'tree_head ofpackaging', '', '', '', '', ''),
(81, 'another test', '1490427598_29073280966-604376461-ticket.pdf', '', '', '', '', '', '', '', '', 'tree_basic', 'tree_CEO', 'tree_head ofpackaging', 'tree_hrad of HR', 'tree_security manager', '', '', ''),
(82, 'Test', '1490441285_1489484845_oop_in_php_tutorial.pdf', '', '', 'blu_head ofpackaging', '', '', '', '', '', '', '', 'tree_head ofpackaging', '', 'tree_security manager', '', '', ''),
(83, 'Chemical Process', '1490442327_1489484082_oop_in_php_tutorial.pdf', '', '', 'blu_head ofpackaging', '', '', 'blu_it manager', '', '', '', '', 'tree_head ofpackaging', '', '', '', '', 'tree_packaging 2'),
(84, 'Testing', '1490442432_1489484845_oop_in_php_tutorial.pdf', '', '', 'blu_head ofpackaging', '', '', '', '', '', 'tree_basic', 'tree_CEO', 'tree_head ofpackaging', '', '', '', '', ''),
(85, 'test test', '1490893423_Delivery_step_van_Hometalk.pdf', '', '', '', '', '', '', '', '', 'tree_basic', 'tree_CEO', 'tree_head ofpackaging', '', '', '', '', ''),
(86, 'BLAA', '1491151379_Delivery_step_van_Hometalk.pdf', '', '', '', '', '', '', '', '', 'tree_basic', 'tree_CEO', '', '', '', '', '', ''),
(87, 'yuyut', '1491151545_Delivery_step_van_Hometalk.pdf', '', '', '', '', '', '', '', '', 'tree_basic', 'tree_CEO', 'tree_head ofpackaging', 'tree_hrad of HR', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pkgs`
--

CREATE TABLE IF NOT EXISTS `tbl_pkgs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo_id` int(11) NOT NULL,
  `que` varchar(200) NOT NULL,
  `que_radio` varchar(200) NOT NULL,
  `a_1` varchar(200) NOT NULL,
  `a_2` varchar(200) NOT NULL,
  `a_3` varchar(200) NOT NULL,
  `a_4` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=133 ;

--
-- Dumping data for table `tbl_pkgs`
--

INSERT INTO `tbl_pkgs` (`id`, `logo_id`, `que`, `que_radio`, `a_1`, `a_2`, `a_3`, `a_4`) VALUES
(112, 72, 'File Treatment is a physical process or not?', '1', 'Yes', 'Not', 'Both', 'No one'),
(113, 73, 'What is File Treament?', '2', 'Washing Files', 'Assumbling Files', 'Creating Files', 'Both A&B '),
(114, 72, 'What Is a Chemical Process?', '4', 'Tube Light', 'Equipment Of Lab', 'Working with Chemicals', 'Both A&B'),
(115, 75, 'What is integible security?', '4', 'Computer Security', 'Physical Security', 'System Security', 'All'),
(116, 74, 'What does it do?', '3', 'good', 'bad', 'something', 'nothing'),
(119, 72, 'FXVCXCVXX', '1', 'XCVXCVXCV', '', '', ''),
(120, 73, 'why bla bla ', '1', 'qwe', 'ert', 'ryu', 'dfg'),
(121, 75, 'werwerwer', '1', 'sdfsdf', 'sdfd', 'sdfsdxcv', 'ergdfgdv'),
(122, 74, 'sdfsdfsd', '4', 'sdfsf', 'sdfsdf', 'sdfsdf', 'sdf'),
(123, 81, 'sdfsdfsdfs', '2', 'sdfs', 'fssdf', 'sdfsdfs', 'dfsfsdfsd'),
(124, 72, 'What is the question? ', '3', 'q1', 'q2', 'q3', 'q4'),
(125, 82, 'What is the question 1?', '1', 'q1', 'q2', '', ''),
(126, 84, 'KJHJCF', '2', 'KHJ', 'jhjh', 'hgv', 'ghgvhg'),
(127, 83, 'mbghgfcgf', '4', 'jbhgv', 'mjbhgf', 'khuyg', 'gygjhbh'),
(128, 84, 'jgtrdf', '3', 'jgyf', 'yfytftgf', 'ytfyfyhf', 'yfkg'),
(129, 85, 'dfgdfg', '3', 'dfgdf', 'gdfgd', 'fgdfg', 'dfgd'),
(130, 85, 'dfg dfg dfg ', '1', 'fdgd', 'fgdfg', 'dfgdf', 'gdfg'),
(131, 86, 'ERWERW', '1', 'WERWE', 'RWERWERW', 'ERWERW', 'ERWERW'),
(132, 87, 'rtert', '1', 'erte', 'terter', 'tertet', 'erterte');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
