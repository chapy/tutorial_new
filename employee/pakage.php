<?php
include("header.php");
if(isset($_SESSION['employee_sess'])){
	 $user_id = $_SESSION['employee_sess']['user_id'];
}
?>
<!--[if !IE]> -->
		<script src="assets/js/jquery-2.1.4.min.js"></script>
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<!--<button class="btn btn-success">
							<i class="ace-icon fa fa-signal"></i>
						</button>

						<button class="btn btn-info">
							<i class="ace-icon fa fa-pencil"></i>
						</button>

						<button class="btn btn-warning">
							<i class="ace-icon fa fa-users"></i>
						</button>

						<button class="btn btn-danger">
							<i class="ace-icon fa fa-cogs"></i>
						</button>-->
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<!--<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>-->
					</div>
				</div><!-- /.sidebar-shortcuts -->


				<ul class="nav nav-list">
					<li class="">
						<a href="index.php">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> ראשי </span>
						</a>

						<b class="arrow"></b>
					</li>
					
					
					<li class="">
						<a href="tables.php">
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text">חבילות למידה</span>
						</a>

						<b class="arrow"></b>
					</li>

					
				</ul><!-- /.nav-list -->
			

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<!--<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">בית</a>
							</li>

							<li>
								<a href="#">UI &amp; Elements</a>
							</li>
							<li class="active">Treeview</li>
						</ul> /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<!--<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>-->
							</form>
						</div><!-- /.nav-search -->
					</div>

					<div class="page-content">
						<div class="ace-settings-container" id="ace-settings-container">
							<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
								<i class="ace-icon fa fa-cog bigger-130"></i>
							</div>

							<div class="ace-settings-box clearfix" id="ace-settings-box">
								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<div class="pull-left">
											<select id="skin-colorpicker" class="hide">
												<option data-skin="no-skin" value="#438EB9">#438EB9</option>
												<option data-skin="skin-1" value="#222A2D">#222A2D</option>
												<option data-skin="skin-2" value="#C6487E">#C6487E</option>
												<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
											</select>
										</div>
										<span>&nbsp; Choose Skin</span>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
										<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
										<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
										<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
										<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
										<label class="lbl" for="ace-settings-add-container">
											Inside
											<b>.container</b>
										</label>
									</div>
								</div><!-- /.pull-left -->

								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
										<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
										<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
										<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
									</div>
								</div><!-- /.pull-left -->
							</div><!-- /.ace-settings-box -->
						</div><!-- /.ace-settings-container -->

						<div class="page-header">
							
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">


							<!-- PAGE CONTENT BEGINS -->

						<div class="row emp" style="width:70%; margin:0 auto;">
						<div class="jumbotron jumb" style="background:#fff">
						<?php 
								$query2 = "select * from `create_account` where `user_id`='$user_id'";
								$rese = mysqli_query($con, $query2);
								$row2 = mysqli_fetch_array($rese);
								
								$uname = $row2['name'];
								if(isset($_GET['tbl_id'])){
								$tbl_id  = $_GET['tbl_id'];
							}
								$query3 = "select * from `tbl_pkg` where `id`='$tbl_id'";
								$res3 = mysqli_query($con, $query3);
								$row3 = mysqli_fetch_array($res3);
								$pdf = $row3['image'];
							?>
							<h2 class="text-center" style="color:#f00;margin-bottom:60px">היי <?php echo $uname; ?></h2>
							<h4 class="text-right"><strong>מבחן על הלומדה</strong></h4>
							<a href="../admin/uploads/<?php echo $pdf; ?>" target="_blank"><img src="images/u1.png" class="img-responsive" style="margin-top: -30px;"></a>
							
						<?php   
							$i=1;
							$j=1;
							if(isset($_GET['tbl_id'])){
								$tbl_id  = $_GET['tbl_id'];
							}
							$query_select= "select * from `tbl_pkgs` where `logo_id`='$tbl_id'";
							 $result=mysqli_query($con,$query_select);
							 $rownum = mysqli_num_rows($result);
							 if($rownum>0){
							while($row=mysqli_fetch_assoc($result)){
								if($i<=30){
							 ?>
						<div class="col-sm-4">
						
						</div>
						<div class="checks text-right col-sm-8">
						<style>
							.ques{
								text-transform: capitalize;
							}
							.ques label{
								font-weight: bold;
							}
						</style>
						<form method="post">
							<input type="hidden" name="questionid[]" value="<?php echo $row['id']; ?>" />
							<input type="hidden" name="ans[]" value="<?php echo $row['que_radio']; ?>" />
							<input type="hidden" name="totalans" value="<?php echo $rownum; ?>" />
						<h5 class="text-right ques"><?php echo $row['que']; ?>	:<label><?php echo $j++;?></label>
						</h5>

						<div class="checkbox">
						<label>
						
						
						<label> <?php echo $row['a_1']; ?> <input type="radio" value="1" name="question<?php echo $row['id']; ?>">
						
						</label>
						
						</div>
						<div class="checkbox">
						  <label> <?php echo $row['a_2']; ?> <input type="radio" value="2" class="pull-right" name="question<?php echo $row['id']; ?>">
						  </label>
						</div>
						<div class="checkbox">
						<label> <?php echo $row['a_3']; ?> <input type="radio" value="3" name="question<?php echo $row['id']; ?>">
						</label>
						</div>
						<div class="checkbox">
						  <label> <?php echo $row['a_4']; ?> <input type="radio" value="4" name="question<?php echo $row['id']; ?>">
						  </label>
						</div>
						</div>
							<?php
							
								}
								
								$i++; 
								
									} 
							 }
							?>
						<div class="form-group pull-right">
							<input type="submit" class="btn btn-success" value="שלח תשובות" name="ans_sub" />
						</div>
						</form>	
													
						</div>
					</div>
									
						
						
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" style="margin-top: 130px;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	  <?php
	    if(isset($_GET['tbl_id'])){
	    $tbl_id  = $_GET['tbl_id'];
							
	  ?>
		<!--<a href="pdf.php?tbl_id=<?php echo $tbl_id; ?>" class="btn btn-warning pull-left">Open PDF</a>-->
		<!--<a href="pdf.php?tbl_id=<?php echo $tbl_id; ?>" class="btn btn-warning pull-left">קרא נוהל</a>-->
        <!--<h3 class="modal-title text-center">Your Test Results</h3>-->
        <h3 class="modal-title text-center">תוצאות הבדיקה שלך</h3>
		<!--<a href="tables.php" class="btn btn-success pull-right" style="margin-top: -30px;">Packages</a>-->
		<a href="tables.php" class="btn btn-success pull-right" style="margin-top: -30px;">חבילות למידה</a>
        
	  <?php } ?>
      </div>
      <div class="modal-body">
			<?php
				$msg = '';
				$message ='';
				if(isset($_GET['tbl_id'])){
					$tbl_id  = $_GET['tbl_id'];
					$_SESSION['pg']=$tbl_id;
					$_SESSION['rep']=$repercent;
				}
				if(isset($_POST['totalans'])){
				$q=0;
				
				$permission = false;
				$resutlqa = array();
				$resutlf = array();
				foreach($_POST['questionid'] as $question){
						$question[$q];
						
				 $sub_ans = $_POST['question'.$question];
				 $totalans = $_POST['totalans'];
				// echo '<br />';
				 $cor_ans = $_POST['ans'][$q];
				// echo '<br />';
				if($sub_ans == $cor_ans){
					$resutlq = 'True';
					$resutlqa[] = '1';
				}else{
					$resutlq = 'False';
					$resutlf[] = '0';
				}
				
				$querycheck = "select * from `final-result` where `userid`='$user_id' and `packageid`='$tbl_id' order by `id` DESC limit 1";
				$recheck = mysqli_query($con,$querycheck);
				
				if( mysqli_num_rows($recheck) > 0 ){
						$rowper = mysqli_fetch_array($recheck);
						$periodb = $rowper['time_period'];
						$lastup  = $rowper['update'];
						$time 	 = strtotime($lastup);
						$curtime = time();
						$effectiveDate = date('d/m/Y', strtotime("+".$periodb."months", strtotime($lastup)));
						
						$effectiveDate = strtotime($effectiveDate);
						
						if( $curtime > $effectiveDate) {     //define time period
						  //do stuff
						
						  $query = "insert into `result` (`user_id`,`question_id`,`result`) values ('$user_id','$question','$resutlq')";
						  mysqli_query($con,$query);
							$permission = true;
							// echo 'true';
						
						}else{
							// echo 'flase';
							
							$permission = false;
						}
						
				
					}else{
						
					$permission = true;

					$query = "insert into `result` (`user_id`,`question_id`,`result`) values ('$user_id','$question','$resutlq')";
					mysqli_query($con,$query);

					}
				$q++;
				
				}
				
				if($permission==true){
				
				$resutlqa = count($resutlqa);
				$resutlf = count($resutlf);
				// $message =  '<div class="col-xs-6 label label-danger"> Your Incorrect Number of Answers: '.$resutlf.'</div>';
				$message =  '<div class="col-xs-6 label label-danger"> מספר תשובות לא נכונות: '.$resutlf.'</div>';
				
				// $message .= '<div class="col-xs-6 label label-success"> Your Correct Number of Answers: '.$resutlqa.'</div> <br /><br />';
				$message .= '<div class="col-xs-6 label label-success"> מספר תשובות נכונות: '.$resutlqa.'</div> <br /><br />';
				
				$repercent = 	($resutlqa/$totalans)*100;
				$repercent = round($repercent,2).'%';
				// $message .= '<div class="col-xs-12 label label-success"> Your Result is: '.$repercent.'</div>';
				$message .= '<div class="col-xs-12 label label-success"> ציונך הוא: '.$repercent.'</div>';
				$message .= '<br />';
				
				}
				
				
				$querycheck = "select * from `final-result` where `userid`='$user_id' and `packageid`='$tbl_id' order by `id` DESC limit 1";
				$recheck = mysqli_query($con,$querycheck);
				
				if(mysqli_num_rows($recheck)>0){
					
						
						$rowper = mysqli_fetch_array($recheck);
						$periodb = $rowper['time_period'];
						$lastup  = $rowper['update'];
						$time 	 = strtotime($lastup);
						$curtime = time();
						$effectiveDate = date('d/m/Y', strtotime("+".$periodb."months", strtotime($lastup)));
						
						$effectiveDate = strtotime($effectiveDate);
						
						if( $curtime > $effectiveDate) {     //define time period
						  //do stuff
						if($repercent>=60){ $period = '12';}else{ $period = '3'; }
						$query = "INSERT INTO `final-result`(`userid`, `result_percent`, `time_period`) VALUES ('$user_id','$repercent','$period')";
						 mysqli_query($con,$query);
						  $permission = true;
						   // echo 'true';
						}else{
						   // echo 'false';
							$permission = false;
						}
						
						
				}else{
						
						$permission = true;
						if($repercent>=60){ $period = '12';}else{ $period = '3'; }	
						$query = "INSERT INTO `final-result`(`userid`, `packageid`, `result_percent`, `time_period`) VALUES ('$user_id','$tbl_id','$repercent','$period')";
						mysqli_query($con,$query);
						
					}
				
				}
					
			?>
			<br /><br />
						<?php 
						if($permission== true){
					if($repercent>=60){
					echo $message;
					// echo '<div class="alert alert-success text-center">You have Passed the Test with average percentage. Your eligible for the next test after 1 year.</div>';
					echo '<div class="alert alert-success text-center">עברת את הבחינה בהצלחה! מועד הבחינה הבא במבחן זה הנו בעוד שנה.</div>';
					
					}else{
					
					echo $message;
					// echo '<div class="alert alert-danger text-center">You have Failed the Test. You are not eligible for the next test after 3 months.</div>';
					echo '<div class="alert alert-danger text-center">נכשלת בבחינה. מועד הבחינה הבא בעוד 3 חודשים.</div>';
					
					}
						}else{
							
							// echo '<div class="alert alert-danger text-center">Your Are Not eligible for '.$periodb.' Months. Your Last Test result Date was: '.$lastup.'</div>';
							echo '<div class="alert alert-danger text-center">אתה לא רשאי לגשת לבחינה במשך '.$periodb.' חודשים. תוצאות הבדיקה האחרונה שלך הייתה: '.$lastup.'</div>';
						}
					
			?>
      </div>
    </div>

  </div>
</div>
<!-- Modal End -->
					
					<?php
	if(isset($_POST['ans_sub'])){
		?>
		<script type="text/javascript">
    $(window).load(function(){
        $('#myModal').modal('show');
    });
</script>
	<?php	
	}
	?>		

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">Ace</span>
							Application &copy; 2013-2014
						</span>

						&nbsp; &nbsp;
						<span class="action-buttons">
							<a href="#">
								<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
							</a>
						</span>
					</div>
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		

		<!-- <![endif]-->

		<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>

		<!-- page specific plugin scripts -->
		<script src="assets/js/tree.min.js"></script>

		<!-- ace scripts -->
		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($){
	var sampleData = initiateDemoData();//see below


	$('#tree1').ace_tree({
		dataSource: sampleData['dataSource1'],
		multiSelect: true,
		cacheItems: true,
		'open-icon' : 'ace-icon tree-minus',
		'close-icon' : 'ace-icon tree-plus',
		'itemSelect' : true,
		'folderSelect': false,
		'selected-icon' : 'ace-icon fa fa-check',
		'unselected-icon' : 'ace-icon fa fa-times',
		loadingHTML : '<div class="tree-loading"><i class="ace-icon fa fa-refresh fa-spin blue"></i></div>'
	});
	
	$('#tree2').ace_tree({
		dataSource: sampleData['dataSource2'] ,
		loadingHTML:'<div class="tree-loading"><i class="ace-icon fa fa-refresh fa-spin blue"></i></div>',
		'open-icon' : 'ace-icon fa fa-folder-open',
		'close-icon' : 'ace-icon fa fa-folder',
		'itemSelect' : true,
		'folderSelect': true,
		'multiSelect': true,
		'selected-icon' : null,
		'unselected-icon' : null,
		'folder-open-icon' : 'ace-icon tree-plus',
		'folder-close-icon' : 'ace-icon tree-minus'
	});
	
	
	/**
	//Use something like this to reload data	
	$('#tree1').find("li:not([data-template])").remove();
	$('#tree1').tree('render');
	*/
	
	
	/**
	//please refer to docs for more info
	$('#tree1')
	.on('loaded.fu.tree', function(e) {
	})
	.on('updated.fu.tree', function(e, result) {
	})
	.on('selected.fu.tree', function(e) {
	})
	.on('deselected.fu.tree', function(e) {
	})
	.on('opened.fu.tree', function(e) {
	})
	.on('closed.fu.tree', function(e) {
	});
	*/
	
	
	function initiateDemoData(){
		var tree_data = {
			'for-sale' : {text: 'For Sale', type: 'folder'}	,
			'vehicles' : {text: 'Vehicles', type: 'folder'}	,
			'rentals' : {text: 'Rentals', type: 'folder'}	,
			'real-estate' : {text: 'Real Estate', type: 'folder'}	,
			'pets' : {text: 'Pets', type: 'folder'}	,
			'tickets' : {text: 'Tickets', type: 'item'}	,
			'services' : {text: 'Services', type: 'item'}	,
			'personals' : {text: 'Personals', type: 'item'}
		}
		tree_data['for-sale']['additionalParameters'] = {
			'children' : {
				'appliances' : {text: 'Appliances', type: 'item'},
				'arts-crafts' : {text: 'Arts & Crafts', type: 'item'},
				'clothing' : {text: 'Clothing', type: 'item'},
				'computers' : {text: 'Computers', type: 'item'},
				'jewelry' : {text: 'Jewelry', type: 'item'},
				'office-business' : {text: 'Office & Business', type: 'item'},
				'sports-fitness' : {text: 'Sports & Fitness', type: 'item'}
			}
		}
		tree_data['vehicles']['additionalParameters'] = {
			'children' : {
				'cars' : {text: 'Cars', type: 'folder'},
				'motorcycles' : {text: 'Motorcycles', type: 'item'},
				'boats' : {text: 'Boats', type: 'item'}
			}
		}
		tree_data['vehicles']['additionalParameters']['children']['cars']['additionalParameters'] = {
			'children' : {
				'classics' : {text: 'Classics', type: 'item'},
				'convertibles' : {text: 'Convertibles', type: 'item'},
				'coupes' : {text: 'Coupes', type: 'item'},
				'hatchbacks' : {text: 'Hatchbacks', type: 'item'},
				'hybrids' : {text: 'Hybrids', type: 'item'},
				'suvs' : {text: 'SUVs', type: 'item'},
				'sedans' : {text: 'Sedans', type: 'item'},
				'trucks' : {text: 'Trucks', type: 'item'}
			}
		}

		tree_data['rentals']['additionalParameters'] = {
			'children' : {
				'apartments-rentals' : {text: 'Apartments', type: 'item'},
				'office-space-rentals' : {text: 'Office Space', type: 'item'},
				'vacation-rentals' : {text: 'Vacation Rentals', type: 'item'}
			}
		}
		tree_data['real-estate']['additionalParameters'] = {
			'children' : {
				'apartments' : {text: 'Apartments', type: 'item'},
				'villas' : {text: 'Villas', type: 'item'},
				'plots' : {text: 'Plots', type: 'item'}
			}
		}
		tree_data['pets']['additionalParameters'] = {
			'children' : {
				'cats' : {text: 'Cats', type: 'item'},
				'dogs' : {text: 'Dogs', type: 'item'},
				'horses' : {text: 'Horses', type: 'item'},
				'reptiles' : {text: 'Reptiles', type: 'item'}
			}
		}

		var dataSource1 = function(options, callback){
			var $data = null
			if(!("text" in options) && !("type" in options)){
				$data = tree_data;//the root tree
				callback({ data: $data });
				return;
			}
			else if("type" in options && options.type == "folder") {
				if("additionalParameters" in options && "children" in options.additionalParameters)
					$data = options.additionalParameters.children || {};
				else $data = {}//no data
			}
			
			if($data != null)//this setTimeout is only for mimicking some random delay
				setTimeout(function(){callback({ data: $data });} , parseInt(Math.random() * 500) + 200);

			//we have used static data here
			//but you can retrieve your data dynamically from a server using ajax call
			//checkout examples/treeview.html and examples/treeview.js for more info
		}




		var tree_data_2 = {
			'pictures' : {text: 'Pictures', type: 'folder', 'icon-class':'red'}	,
			'music' : {text: 'Music', type: 'folder', 'icon-class':'orange'}	,
			'video' : {text: 'Video', type: 'folder', 'icon-class':'blue'}	,
			'documents' : {text: 'Documents', type: 'folder', 'icon-class':'green'}	,
			'backup' : {text: 'Backup', type: 'folder'}	,
			'readme' : {text: '<i class="ace-icon fa fa-file-text grey"></i> ReadMe.txt', type: 'item'},
			'manual' : {text: '<i class="ace-icon fa fa-book blue"></i> Manual.html', type: 'item'}
		}
		tree_data_2['music']['additionalParameters'] = {
			'children' : [
				{text: '<i class="ace-icon fa fa-music blue"></i> song1.ogg', type: 'item'},
				{text: '<i class="ace-icon fa fa-music blue"></i> song2.ogg', type: 'item'},
				{text: '<i class="ace-icon fa fa-music blue"></i> song3.ogg', type: 'item'},
				{text: '<i class="ace-icon fa fa-music blue"></i> song4.ogg', type: 'item'},
				{text: '<i class="ace-icon fa fa-music blue"></i> song5.ogg', type: 'item'}
			]
		}
		tree_data_2['video']['additionalParameters'] = {
			'children' : [
				{text: '<i class="ace-icon fa fa-film blue"></i> movie1.avi', type: 'item'},
				{text: '<i class="ace-icon fa fa-film blue"></i> movie2.avi', type: 'item'},
				{text: '<i class="ace-icon fa fa-film blue"></i> movie3.avi', type: 'item'},
				{text: '<i class="ace-icon fa fa-film blue"></i> movie4.avi', type: 'item'},
				{text: '<i class="ace-icon fa fa-film blue"></i> movie5.avi', type: 'item'}
			]
		}
		tree_data_2['pictures']['additionalParameters'] = {
			'children' : {
				'wallpapers' : {text: 'Wallpapers', type: 'folder', 'icon-class':'pink'},
				'camera' : {text: 'Camera', type: 'folder', 'icon-class':'pink'}
			}
		}
		tree_data_2['pictures']['additionalParameters']['children']['wallpapers']['additionalParameters'] = {
			'children' : [
				{text: '<i class="ace-icon fa fa-picture-o green"></i> wallpaper1.jpg', type: 'item'},
				{text: '<i class="ace-icon fa fa-picture-o green"></i> wallpaper2.jpg', type: 'item'},
				{text: '<i class="ace-icon fa fa-picture-o green"></i> wallpaper3.jpg', type: 'item'},
				{text: '<i class="ace-icon fa fa-picture-o green"></i> wallpaper4.jpg', type: 'item'}
			]
		}
		tree_data_2['pictures']['additionalParameters']['children']['camera']['additionalParameters'] = {
			'children' : [
				{text: '<i class="ace-icon fa fa-picture-o green"></i> photo1.jpg', type: 'item'},
				{text: '<i class="ace-icon fa fa-picture-o green"></i> photo2.jpg', type: 'item'},
				{text: '<i class="ace-icon fa fa-picture-o green"></i> photo3.jpg', type: 'item'},
				{text: '<i class="ace-icon fa fa-picture-o green"></i> photo4.jpg', type: 'item'},
				{text: '<i class="ace-icon fa fa-picture-o green"></i> photo5.jpg', type: 'item'},
				{text: '<i class="ace-icon fa fa-picture-o green"></i> photo6.jpg', type: 'item'}
			]
		}


		tree_data_2['documents']['additionalParameters'] = {
			'children' : [
				{text: '<i class="ace-icon fa fa-file-text red"></i> document1.pdf', type: 'item'},
				{text: '<i class="ace-icon fa fa-file-text grey"></i> document2.doc', type: 'item'},
				{text: '<i class="ace-icon fa fa-file-text grey"></i> document3.doc', type: 'item'},
				{text: '<i class="ace-icon fa fa-file-text red"></i> document4.pdf', type: 'item'},
				{text: '<i class="ace-icon fa fa-file-text grey"></i> document5.doc', type: 'item'}
			]
		}

		tree_data_2['backup']['additionalParameters'] = {
			'children' : [
				{text: '<i class="ace-icon fa fa-archive brown"></i> backup1.zip', type: 'item'},
				{text: '<i class="ace-icon fa fa-archive brown"></i> backup2.zip', type: 'item'},
				{text: '<i class="ace-icon fa fa-archive brown"></i> backup3.zip', type: 'item'},
				{text: '<i class="ace-icon fa fa-archive brown"></i> backup4.zip', type: 'item'}
			]
		}
		var dataSource2 = function(options, callback){
			var $data = null
			if(!("text" in options) && !("type" in options)){
				$data = tree_data_2;//the root tree
				callback({ data: $data });
				return;
			}
			else if("type" in options && options.type == "folder") {
				if("additionalParameters" in options && "children" in options.additionalParameters)
					$data = options.additionalParameters.children || {};
				else $data = {}//no data
			}
			
			if($data != null)//this setTimeout is only for mimicking some random delay
				setTimeout(function(){callback({ data: $data });} , parseInt(Math.random() * 500) + 200);

			//we have used static data here
			//but you can retrieve your data dynamically from a server using ajax call
			//checkout examples/treeview.html and examples/treeview.js for more info
		}

		
		return {'dataSource1': dataSource1 , 'dataSource2' : dataSource2}
	}

});
		</script>
	</body>
</html>
