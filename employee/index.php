<!DOCTYPE html>
<html lang="he">
    <head>
        <meta charset="utf-8">
	</head>
</html>
<?php
include("header.php");
if(isset($_SESSION['employee_sess'])){
	 $user_id = $_SESSION['employee_sess']['user_id'];
}
?>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>
</script>
	 <script type="text/javascript" src="js/canvasjs.min.js"></script>
	 <?php 
	$user_type = $_SESSION['employee_sess']['role'];
	$j=1;
	$user_type1='blu_'.$user_type;
	$user_type2='tree_'.$user_type;
	// $query= "select * from `tbl_pkg` where b_hop='$user_type1'";
	$query= "select * from `tbl_pkg` where b_basic='$user_type1' || b_CEO='$user_type1' || b_hop='$user_type1' || b_hrad_HR='$user_type1' || b_sm='$user_type1' || b_itm='$user_type1' || b_pw='$user_type1' || b_p2='$user_type1' || t_basic='$user_type2' || t_CEO='$user_type2' || t_hop='$user_type2' || t_hrad='$user_type2' || t_sm='$user_type2' || t_itm='$user_type2' || t_pw='$user_type2' || t_p2='$user_type2'";
	 $result=mysqli_query($con,$query);
	$i=1;
	$total_stt2=0;
	$total_assign = mysqli_num_rows($result);
	 while($row=mysqli_fetch_assoc($result)){
	$package_id = $row['id'];
	$query_stt2 = "select * from `final-result` where `userid`='$user_id' and `packageid`='$package_id'";
	$res_stt2 = mysqli_query($con, $query_stt2);
	$res_t = mysqli_num_rows($res_stt2);
	$total_stt2+=$res_t;
	
	 $i++; }
	$total_assign;
	$total_stt2;
	$pre = $total_assign-$total_stt2;
	$total_done = round($total_done = (($total_stt2/$total_assign)*100), 2);
	$remaining  = round($remaining = (100-$total_done), 2);
	
  ?>
	<script type="text/javascript">
		window.onload = function () {
			var chartA = new CanvasJS.Chart("chartContainer3", {
				title: {
					text: "Site Traffic",
					fontSize: 30
				},
				animationEnabled: true,
				axisX: {
					gridColor: "Silver",
					tickColor: "silver",
					valueFormatString: "DD/MMM"
				},
				toolTip: {
					shared: true
				},
				theme: "theme2",
				axisY: {
					gridColor: "Silver",
					tickColor: "silver"
				},
				legend: {
					verticalAlign: "center",
					horizontalAlign: "right"
				},
				data: [
				{
					type: "line",
					showInLegend: true,
					lineThickness: 2,
					name: "Result",
					markerType: "square",
					color: "#F08080",
					dataPoints: [
					{ x: new Date(2010, 0, 3), y: 650 },
					{ x: new Date(2010, 0, 5), y: 700 },
					{ x: new Date(2010, 0, 7), y: 710 },
					{ x: new Date(2010, 0, 9), y: 658 },
					{ x: new Date(2010, 0, 11), y: 734 },
					{ x: new Date(2010, 0, 13), y: 963 },
					{ x: new Date(2010, 0, 15), y: 847 },
					{ x: new Date(2010, 0, 17), y: 853 },
					{ x: new Date(2010, 0, 19), y: 869 },
					{ x: new Date(2010, 0, 21), y: 943 },
					{ x: new Date(2010, 0, 23), y: 970 }
					]
				},
				{
					// type: "line",
					// showInLegend: true,
					// name: "Hebrew",
					// color: "#20B2AA",
					// lineThickness: 2,

					dataPoints: [
					{ x: new Date(2010, 0, 3), y: 510 },
					{ x: new Date(2010, 0, 5), y: 560 },
					{ x: new Date(2010, 0, 7), y: 540 },
					{ x: new Date(2010, 0, 9), y: 558 },
					{ x: new Date(2010, 0, 11), y: 544 },
					{ x: new Date(2010, 0, 13), y: 693 },
					{ x: new Date(2010, 0, 15), y: 657 },
					{ x: new Date(2010, 0, 17), y: 663 },
					{ x: new Date(2010, 0, 19), y: 639 },
					{ x: new Date(2010, 0, 21), y: 673 },
					{ x: new Date(2010, 0, 23), y: 660 }
					]
				}
				],
				legend: {
					cursor: "pointer",
					itemclick: function (e) {
						if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
							e.dataSeries.visible = false;
						}
						else {
							e.dataSeries.visible = true;
						}
						chartA.render();
					}
				}
			});

			// chartA.render();
		
		
    var chartB = new CanvasJS.Chart("chartContainer",
    {
      title:{
      text: "התקדמות נוהלי עבודה"   
      },
      animationEnabled: true,
      axisY:{
        title: ""
      },
      data: [
      {        
        type: "stackedColumn100",
        name: "לומדות שבוצעו",
        showInLegend: "true",
  color:"rgb(68,114,196)",
  
        dataPoints: [
        {  y: <?php echo$total_done; ?>, label: "לומדות שבוצעו"},                
        ]
      }, {        
        type: "stackedColumn100",        
        name: "לומדות שנשארו לביצוע",
        showInLegend: "true",
  color:"rgb(237,125,49)",
        dataPoints: [
        {  y: <?php echo $remaining; ?>, label: "לומדות שנשארו לביצוע"},
                
        ]
      }

      ]
    });

    chartB.render();
  }
		
  </script>
				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<!--<button class="btn btn-success">
							<i class="ace-icon fa fa-signal"></i>
						</button>

						<button class="btn btn-info">
							<i class="ace-icon fa fa-pencil"></i>
						</button>

						<button class="btn btn-warning">
							<i class="ace-icon fa fa-users"></i>
						</button>

						<button class="btn btn-danger">
							<i class="ace-icon fa fa-cogs"></i>
						</button>-->
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<!--<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>-->
					</div>
				</div><!-- /.sidebar-shortcuts -->
				
				<ul class="nav nav-list">
					<li class="">
						<a href="index.php">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> ראשי </span>
						</a>

						<b class="arrow"></b>
					</li>
					
					
					<li class="">
						<a href="tables.php">
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text">חבילות למידה</span>
						</a>

						<b class="arrow"></b>
					</li>
					
				</ul><!-- /.nav-list -->

				</div><!-- /.main-container -->
					<!--main content -->
					
					
						
							<div class="col-sm-8 center">
							<?php 
								$query2 = "select * from `create_account` where `user_id`='$user_id'";
								$res2 = mysqli_query($con, $query2);
								$row2 = mysqli_fetch_array($res2);
								$uname = $row2['name'];
							?>
							<h2 class="text-center">היי <?php echo $uname; ?></h2>
							<?php 
								// $queryp = "select * from `tbl_pkg` where `status`='0'";
								// $res = mysqli_query($con, $queryp);
								// $rowp = mysqli_fetch_array($res);
								// if($numr = mysqli_num_rows($res)>0){
							?>
						 <!--h4 class="text-center" style="padding-top:20px">You have <?php echo $numr; ?> packages to read and answer</h4-->
							<?php 
								// }else{
								 // $user_type = $_SESSION['employee_sess']['role'];
								// $i=1;
								// $j=1;
								// $user_type1='blu_'.$user_type;
								// $user_type2='tree_'.$user_type;
								// $query= "select * from `tbl_pkg` where b_hop='$user_type1'";
								// $query= "select * from `tbl_pkg` where b_basic='$user_type1' || b_CEO='$user_type1' || b_hop='$user_type1' || b_hrad_HR='$user_type1' || b_sm='$user_type1' || b_itm='$user_type1' || b_pw='$user_type1' || b_p2='$user_type1' || t_basic='$user_type2' || t_CEO='$user_type2' || t_hop='$user_type2' || t_hrad='$user_type2' || t_sm='$user_type2' || t_itm='$user_type2' || t_pw='$user_type2' || t_p2='$user_type2'";
								 // $result=mysqli_query($con,$query);
								 // $row=mysqli_fetch_assoc($result);
								
								 // {
									// $total_assign = mysqli_num_rows($result);
									// $query_stt2 = "select * from `result` where `user_id`='$user_id'";
									// $res_stt2 = mysqli_query($con, $query_stt2);
									 // $res_t = mysqli_num_rows($res_stt2);
									 // $total_assign;
									 // $remaining = ($total_assign-$res_t);
									 // $remaining;
								?>
							<h4 class="text-center" style="padding-top:20px">נשארו לך <?php echo $pre; ?> חבילות למידה לקרוא ולהיבחן</h4>
							
							<div class="col-sm-6 pad">
								<div id="chartContainer" style="height:50%;width: 100%">
								</div>
								<!--img src="images/time.png" class="img-responsive"-->
							</div>
							<div class="col-sm-6 pad">
							<?php 
								// $queryp = "select * from `tbl_pkg` where `status`='1'";
								// $res = mysqli_query($con, $queryp);
								// $rowp = mysqli_fetch_array($res);
								// if($numr2 = mysqli_num_rows($res)>0){
							?>
							<?php 
								// }else{
							?>	
								<h4 class="text-right">בוצעו: <?php if($total_stt2 != 0){ echo $total_stt2; }else{ echo '0';} ?></h4>
							<?php
								// }
							?>
							 <h4 class="text-right tope">נשארו לביצוע: <?php echo $pre; ?> 
							 </h4>
							<div class="btttn">
							<a href="tables.php" style = "background:green !important" class="btn btn-default btn-lg">צפה בחבילות עבודה</a>
							</div>
							</div>
							<hr>
							<div class="col-sm-12" style="margin-top: 80px;">
							<br />
							<br />
							<br />
							<!--img src="images/map.jpg" class="img-responsive map"-->
							<div id="chartContainer3" style="height: 400px; width: 100%;margin-top: 40px;"></div>
							</div>
							</div>
							<div class="clearfix"></div>
							
							
<?php
include("footer.php");
?>